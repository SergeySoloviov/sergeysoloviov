package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[][] pyramid = fill(inputNumbers, check(inputNumbers));
        return pyramid;
    }

    private int[][] fill(List<Integer> inputNumbers, int height) {
        int width = 2 * height - 1;
        int[][] pyramid = new int[height][width];
        int index = 0;
        for (int i = 0; i < height; i++) {
            int k = height - i - 1;
            for (int j = 0; j <= i; j++) {

                pyramid[i][k] = inputNumbers.get(index++);
                k += 2;
            }
        }
        return pyramid;
    }

    private int check(List<Integer> inputNumbers) {
        try {
            Collections.sort(inputNumbers);
        } catch (Error | Exception e) {
            throw new CannotBuildPyramidException("It is not possible to build pyramid");
        }
        int sum = 0;
        for (int i = 1; i < inputNumbers.size(); i++) {
            sum += i;
            if (sum == inputNumbers.size()) {
                return i;
            }
        }
        throw new CannotBuildPyramidException("It is not possible to build pyramid");
    }
}