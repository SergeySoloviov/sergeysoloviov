package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        LinkedList<Double> value = new LinkedList<>();
        LinkedList<Character> operators = new LinkedList<Character>();
        try {
            for (int i = 0; i < statement.length(); i++) {
                char ch = statement.charAt(i);
                if (ch == '(') {
                    operators.add('(');
                } else if (ch == ')') {
                    while (operators.getLast() != '(') {
                        math(value, operators.removeLast());
                    }
                    operators.removeLast();
                } else if (isOperator(ch)) {
                    while (!operators.isEmpty() && priority(operators.getLast()) >= priority(ch)) {
                        math(value, operators.removeLast());
                    }
                    operators.add(ch);
                } else if (Character.isDigit(ch)) {
                    String operand = "";
                    while (i < statement.length() &&
                            (statement.charAt(i) == '.' || Character.isDigit(statement.charAt(i)) || statement.charAt(i) == ',')) {
                        if (statement.charAt(i) == ',') {
                            return null;
                        }
                        operand += statement.charAt(i++);
                    }
                    --i;
                    try {
                        value.add(Double.parseDouble(operand));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                }
            }
            while (!operators.isEmpty()) {
                math(value, operators.removeLast());
            }
            NumberFormat numberFormat = NumberFormat.getInstance(Locale.US);
            numberFormat.setMaximumFractionDigits(4);
            return numberFormat.format(value.get(0));
        } catch (Exception e) {
            return null;
        }
    }

    private void math(LinkedList<Double> expr, char operation) {
        double rightOperand = expr.removeLast();
        double leftOperand = expr.removeLast();
        switch (operation) {
            case '+':
                expr.add(leftOperand + rightOperand);
                break;
            case '-':
                expr.add(leftOperand - rightOperand);
                break;
            case '*':
                expr.add(leftOperand * rightOperand);
                break;
            case '/':
                if (rightOperand == 0) {
                    throw new ArithmeticException();
                }
                expr.add(leftOperand / rightOperand);
                break;
        }
    }

    private int priority(char operator) {
        if (operator == '*' || operator == '/') {
            return 1;
        } else if (operator == '+' || operator == '-') {
            return 0;
        } else {
            return -1;
        }
    }

    private boolean isOperator(char ch) {
        return ch == '+' || ch == '-' || ch == '*' || ch == '/';
    }
}